====================================== CONTENT ======================================
  This project will display a grid MxN with 3 possible values for each cell:
* 0 (dead) = #a3a3a3
* 1 (sad) = #5daaff
* 2 (happy) = #2cb48a

  The application will display a START and PAUSE button on top of the page. Once the
START button is clicked, it will perform a change in each cell every second according
to the following rules:

1. Any sad or happy cell with a combined total of exactly two or three sad or happy neighbors survives.
2. Any dead cell with exactly three sad neighbors becomes a sad cell.
3. Any dead cell with exactly two sad neighbors and 1 happy neighbor becomes a sad cell.
4. Any dead cell with exactly one sad neighbor and 2 happy neighbors becomes a happy cell.
5. Any dead cell with exactly 3 happy neighbors becomes a happy cell.
6. All other cells become or remain dead.

When the PAUSE button is pressed, the animation should stop.


====================================== HOW TO RUN ======================================
  Run the following commands on the terminal to run the application:
    yarn
    yarn start