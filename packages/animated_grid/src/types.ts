export type GridInfoProps = {
  m: number;
  n: number;
  state: number[][];
};
