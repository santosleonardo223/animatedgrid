export enum CellStates {
  DEAD,
  SAD,
  HAPPY,
}

const SERVER_URL = 'http://localhost:4000';

export { SERVER_URL };
