import theme from '../theme';

const getCellColor = (value: number): string => {
  switch (value) {
    case 0:
      return theme.colors.dead;

    case 1:
      return theme.colors.sad;

    case 2:
      return theme.colors.happy;

    default:
      return theme.colors.white;
  }
};

export default getCellColor;
