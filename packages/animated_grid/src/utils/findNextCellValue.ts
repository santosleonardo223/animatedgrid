import { CellStates } from '../constants';

type GridElementInfo = {
  grid: number[][];
  x: number;
  y: number;
};

type CellNeighboursAmounts = {
  [state: number]: number;
};

const getCellNeighbours = ({
  grid,
  x,
  y,
}: GridElementInfo): CellNeighboursAmounts => {
  const neighbours: CellNeighboursAmounts = {
    [CellStates.DEAD]: 0, //dead
    [CellStates.SAD]: 0, //sad
    [CellStates.HAPPY]: 0, //happy
  };

  for (let i = x - 1; i <= x + 1; i++) {
    if (i < 0 || i >= grid.length) {
      continue;
    }

    for (let j = y - 1; j <= y + 1; j++) {
      if (j < 0 || j >= grid[0].length || (i === x && j === y)) {
        continue;
      }

      neighbours[grid[i][j]]++;
    }
  }

  return neighbours;
};

const findNextCellValue = ({ grid, x, y }: GridElementInfo): number => {
  const cellNeighbours = getCellNeighbours({
    grid,
    x,
    y,
  });
  let newState: CellStates = CellStates.DEAD;

  if (
    (grid[x][y] === CellStates.SAD || grid[x][y] === CellStates.HAPPY) &&
    ((cellNeighbours[CellStates.SAD] >= 2 &&
      cellNeighbours[CellStates.SAD] <= 3) ||
      (cellNeighbours[CellStates.HAPPY] >= 2 &&
        cellNeighbours[CellStates.HAPPY] <= 3))
  ) {
    newState = grid[x][y];
  } else if (grid[x][y] === CellStates.DEAD) {
    if (cellNeighbours[CellStates.SAD] === 3) {
      newState = CellStates.SAD;
    } else if (
      cellNeighbours[CellStates.SAD] === 2 &&
      cellNeighbours[CellStates.HAPPY] === 1
    ) {
      newState = CellStates.SAD;
    } else if (
      cellNeighbours[CellStates.SAD] === 1 &&
      cellNeighbours[CellStates.HAPPY] === 2
    ) {
      newState = CellStates.HAPPY;
    } else if (cellNeighbours[CellStates.HAPPY] === 3) {
      newState = CellStates.HAPPY;
    }
  }

  return newState;
};

export { findNextCellValue };
