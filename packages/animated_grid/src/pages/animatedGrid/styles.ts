import styled from 'styled-components';

import getCellColor from '../../utils/getCellColor';
import { Theme } from '../../theme';

const Container = styled.div`
  padding: ${({ theme: { spacing } }: { theme: Theme }) =>
    `${spacing.default} ${spacing.small}`};
`;

const ButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  padding: ${({ theme: { spacing } }: { theme: Theme }) =>
    `${spacing.default} ${spacing.medium}`};
  margin-bottom: ${({ theme: { spacing } }: { theme: Theme }) =>
    spacing.default};

  button {
    color: ${({ theme: { colors } }: { theme: Theme }) => colors.white};
    padding: ${({ theme: { spacing } }: { theme: Theme }) => spacing.medium};
    font-weight: bold;
    border-radius: 50%;
    cursor: pointer;
  }

  button:disabled {
    cursor: default;
    color: ${({ theme: { colors } }: { theme: Theme }) => colors.gray};
    background-color: ${({ theme: { colors } }: { theme: Theme }) =>
      colors.lightGray};
  }

  button:hover :not(:disabled) {
    border: 2px solid black;
  }

  button:not(:first-child) {
    margin-left: ${({ theme }: { theme: Theme }) => theme.spacing.medium};
  }

  .start-button {
    background-color: ${({ theme: { colors } }: { theme: Theme }) =>
      colors.blue};
  }

  .pause-button {
    background-color: ${({ theme: { colors } }: { theme: Theme }) =>
      colors.red};
  }
`;

const Grid = styled.div<{ m?: number; n?: number }>`
  display: grid;
  grid-gap: ${({ theme }: { theme: Theme }) => theme.spacing.small};
  grid-template-rows: ${({ m }: { m?: number }) => `repeat(${m ?? 0}, 1fr)`};
  grid-template-columns: ${({ n }: { n?: number }) => `repeat(${n ?? 0}, 1fr)`};
`;

const Cell = styled.div<{ value: number }>`
  text-align: center;
  padding: ${({ theme }: { theme: Theme }) => theme.spacing.xSmall};
  background-color: ${({ value }: { value: number }) => getCellColor(value)};
`;

export { Container, ButtonWrapper, Grid, Cell };
