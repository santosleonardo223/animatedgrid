import React, { useEffect, useState } from 'react';

import { Container, Grid, Cell, ButtonWrapper } from './styles';
import { GridInfoProps } from '../../types';
import { findNextCellValue } from '../../utils/findNextCellValue';
import { SERVER_URL } from '../../constants';

const AnimatedGrid = () => {
  const [grid, setGrid] = useState<GridInfoProps | null>(null);
  const [currGrid, setCurrGrid] = useState<number[][] | null>(null);
  const [intervalId, setIntervalId] = useState<NodeJS.Timer | null>(null);
  const [isRunning, setIsRunning] = useState(false);

  const startGridAnimation = () => {
    const timeId = setInterval(() => {
      setCurrGrid((prevState) => {
        if (prevState) {
          const newGrid = prevState.map((row, x) =>
            row.map((_, y) => findNextCellValue({ grid: prevState, x, y }))
          );
          return newGrid;
        }

        return prevState;
      });
    }, 1000);

    setIntervalId(timeId);
    setIsRunning(true);
  };

  const pauseGridAnimation = () => {
    setIsRunning(false);
    if (intervalId) {
      clearInterval(intervalId);
    }
  };

  useEffect(() => {
    fetch(`${SERVER_URL}/grid/`)
      .then((res) => res.json())
      .then((gridInfo: GridInfoProps) => {
        console.log(gridInfo);
        setGrid(gridInfo);
        setCurrGrid(gridInfo.state);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <Container>
      <ButtonWrapper>
        <button
          className={'start-button'}
          onClick={startGridAnimation}
          disabled={isRunning}
        >
          Start
        </button>
        <button
          className={'pause-button'}
          onClick={pauseGridAnimation}
          disabled={!isRunning}
        >
          Pause
        </button>
      </ButtonWrapper>

      {grid && currGrid && (
        <Grid m={grid.m} n={grid.n}>
          {currGrid.map((row, m) =>
            row.map((element, n) => (
              <Cell key={`${m}_${n}`} value={element}>
                {element}
              </Cell>
            ))
          )}
        </Grid>
      )}
    </Container>
  );
};

export default AnimatedGrid;
