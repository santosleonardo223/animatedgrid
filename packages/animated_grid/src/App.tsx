import React from 'react';
import { createGlobalStyle, ThemeProvider } from 'styled-components';

import AppRouter from './router';
import theme from './theme';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0px;
    padding: 0px;
    font-family: ${theme.font.family};
  }
`;

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <AppRouter />
    </ThemeProvider>
  );
};

export default App;
