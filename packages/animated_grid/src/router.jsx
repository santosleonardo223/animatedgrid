import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import AnimatedGrid from './pages/AnimatedGrid';

const AppRouter = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" component={AnimatedGrid} />
    </Switch>
  </BrowserRouter>
);

export default AppRouter;
