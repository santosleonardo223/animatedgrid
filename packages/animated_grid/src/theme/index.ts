const theme = {
  colors: {
    black: '#000000',
    blue: '#0000ff',
    dead: '#a3a3a3',
    gray: '#818181',
    happy: '#2cb48a',
    lightGray: '#FAFAFA',
    red: '#E93E4F',
    sad: '#5daaff',
    white: '#ffffff',
  },
  spacing: {
    xSmall: '4px',
    small: '8px',
    default: '16px',
    medium: '24px',
    large: '32px',
  },
  font: {
    family: 'CoreSansA, Helvetica, Arial, sans-serif',
    size: {
      default: '16px',
      large: '24px',
    },
  },
};

export type Theme = typeof theme;

export default theme;
