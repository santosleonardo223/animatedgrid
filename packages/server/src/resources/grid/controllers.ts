import { Request, Response } from 'express';

const { generateGrid } = require('./utils');

const getGrid = (_: Request, res: Response) => {
  res.status(200).json(generateGrid());
};

module.exports = { getGrid };
