const { Router } = require('express');

const { getGrid } = require('./controllers');

const router = Router();

router.get('/', getGrid);

module.exports = router;
