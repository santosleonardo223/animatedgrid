const MAX_ROW_SIZE = 50;
const MAX_COLUMN_SIZE = 50;
const CELL_MAX_VALUE = 2;

type GridInfoProps = {
  m: number;
  n: number;
  state: number[][];
};

const generateGrid = (): GridInfoProps => {
  const m = Math.floor(Math.random() * MAX_ROW_SIZE);
  const n = Math.floor(Math.random() * MAX_COLUMN_SIZE);
  const grid: number[][] = [];

  for (let i = 0; i < m; i++) {
    const row: number[] = [];

    for (let j = 0; j < n; j++) {
      row.push(Math.floor(Math.random() * (CELL_MAX_VALUE + 1)));
    }

    grid.push(row);
  }

  return { m, n, state: grid };
};

module.exports = { generateGrid };
