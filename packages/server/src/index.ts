const express = require('express');
const cors = require('cors');
const { json, urlencoded } = require('body-parser');
const morgan = require('morgan');

const GridRouter = require('./resources/grid/router');

const PORT = 4000;
const app = express();

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));
app.use(morgan('tiny'));

app.use('/grid', GridRouter);

app.listen(PORT, () => {
  console.log(`Server started on http://localhost:${PORT}`);
});
